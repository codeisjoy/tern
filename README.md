# Tern #
## The Flickr Photo Discovery ##

This is a mobile application has been written for REFFIND as a coding test.

The first page shows up to 20 public photos have been added recently. Also, user can search for photos and see up to 500 public photos sorted by date of post ascending.

By selecting a thumbnail, a large instance of the photo starts downloading. In addition, the owner name, photo title and the date photo has been added to system are on the screen.

### Features ###

* Caching photos
* Pull-to-refresh enabled collection views
* Double tap on large view for smart zoom in/out on 

### Sample Shots ###

![Simulator-Screen-Shot-4-Nov-2015,-9.20.04-AM.jpg](https://bitbucket.org/repo/KKjyMz/images/3793833440-Simulator-Screen-Shot-4-Nov-2015,-9.20.04-AM.jpg)
![Simulator-Screen-Shot-4-Nov-2015,-9.20.59-AM.jpg](https://bitbucket.org/repo/KKjyMz/images/3628370569-Simulator-Screen-Shot-4-Nov-2015,-9.20.59-AM.jpg)