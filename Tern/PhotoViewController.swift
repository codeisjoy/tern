//
//  PhotoViewController.swift
//  Tern
//
//  Created by Emad A on 2/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

class PhotoViewController: UIViewController {
    
    var photo: Photo?
    
    @IBOutlet var scrollView: UIScrollView?
    @IBOutlet var translucentCoveringView: UIView?
    @IBOutlet var loadingView: UIActivityIndicatorView?
    
    @IBOutlet var photoOwnerLabel: UILabel?
    @IBOutlet var photoTitleLabel: UILabel?
    @IBOutlet var photoDateLabel: UILabel?
    
    private var imageView = UIImageView()
    
    private var downloadTask: NSURLSessionDownloadTask?
    
    // MARK: - Overriden Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = UIColor.blackColor()
        
        if let photo = photo {
            photoDateLabel?.text = dateString(forDate: photo.dateUploaded)
            photoTitleLabel?.text = photo.title
            photoOwnerLabel?.text = photo.owner.realName
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if let photo = photo {
            let cachDirectoryPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true).last
            let cachedFilePath = NSURL(string: cachDirectoryPath!)!.URLByAppendingPathComponent(photo.uid+"__large").absoluteString
            
            // If given photo has a cached thumbnail then set use the cashed file
            if NSFileManager.defaultManager().fileExistsAtPath(cachedFilePath) {
                completeDownloadTask(UIImage(contentsOfFile: cachedFilePath))
                return
            }
            
            loadingView?.startAnimating()
            
            let session = NSURLSession.sharedSession()
            downloadTask = session.downloadTaskWithURL(photo.largeViewURL, completionHandler: { [unowned self] tmpURL, response, error in
                guard error == nil else {
                    return
                }
                
                if let imageURL = tmpURL, imageData = NSData(contentsOfURL: imageURL) {
                    imageData.writeToFile(cachedFilePath, atomically: true)
                    dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                        self.loadingView?.stopAnimating()
                        self.completeDownloadTask(UIImage(data: imageData))
                    }
                }
                })
            downloadTask?.resume()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if downloadTask?.state == .Running {
            downloadTask?.cancel()
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        scrollView?.frame = view.bounds
        
        guard let
            container = translucentCoveringView,
            ownerLabel = photoOwnerLabel,
            titleLabel = photoTitleLabel,
            dateLabel = photoDateLabel
        else {
            return
        }
        
        var tmpHeight: CGFloat = 0
        
        let offset = UIOffset(horizontal: 20, vertical: 5)
        let constrainedWidth = container.bounds.width - offset.horizontal * 2
        
        tmpHeight = dateLabel.text?.heightWithConstrainedWidth(constrainedWidth, font: dateLabel.font) ?? 0
        dateLabel.frame = CGRect(
            origin: CGPoint(x: offset.horizontal, y: CGRectGetMaxY(container.bounds) - tmpHeight - offset.vertical - 20),
            size: CGSize(width: constrainedWidth, height: tmpHeight))
        
        tmpHeight = titleLabel.text?.heightWithConstrainedWidth(constrainedWidth, font: titleLabel.font) ?? 0
        titleLabel.frame = CGRect(
            origin: CGPoint(x: offset.horizontal, y: CGRectGetMinY(dateLabel.frame) - tmpHeight - offset.vertical),
            size: CGSize(width: constrainedWidth, height: tmpHeight))
        
        tmpHeight = ownerLabel.text?.heightWithConstrainedWidth(constrainedWidth, font: ownerLabel.font) ?? 0
        ownerLabel.frame = CGRect(
            origin: CGPoint(x: offset.horizontal, y: CGRectGetMinY(titleLabel.frame) - tmpHeight - offset.vertical),
            size: CGSize(width: constrainedWidth, height: tmpHeight))
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    override func preferredStatusBarUpdateAnimation() -> UIStatusBarAnimation {
        return .Slide
    }
    
    @IBAction func dismiss() {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Private Methods
    
    private func completeDownloadTask(image: UIImage?) {
        guard let scv = scrollView, let img = image else {
            return
        }
        
        // Set up the image view and the scroller regarding the given image
        
        imageView.image = img
        imageView.sizeToFit()
        
        scv.contentSize = img.size
        
        // Define the scroll view min, max and current scale value
        
        let scale = min(
            scv.bounds.width / scv.contentSize.width,
            scv.bounds.height / scv.contentSize.height)
        
        scv.minimumZoomScale = scale
        scv.zoomScale = scale
        
        // Bring the image view into view
        
        imageView.alpha = 0
        scv.addSubview(imageView)
        
        UIView.animateWithDuration(0.25) { [unowned self] in
            self.imageView.alpha = 1
        }
        
        // Add a tap gesture recognizer to zoom in/out when scroll view double tapped
        
        let doubleTapRecognizer = UITapGestureRecognizer(target: self, action: "handleScrollViewDoubleTap:")
        doubleTapRecognizer.numberOfTouchesRequired = 1
        doubleTapRecognizer.numberOfTapsRequired = 2
        scv.addGestureRecognizer(doubleTapRecognizer)
    }
    
    private func centerScrollViewContent() {
        var contentsFrame = imageView.frame
        let boundsSize = scrollView?.bounds.size ?? CGSizeZero
        
        contentsFrame.origin = CGPoint(
            x: (contentsFrame.width  < boundsSize.width)  ? ((boundsSize.width  - contentsFrame.width)  / 2) : 0,
            y: (contentsFrame.height < boundsSize.height) ? ((boundsSize.height - contentsFrame.height) / 2) : 0)
        
        imageView.frame = contentsFrame
    }
    
    private func setTranslucentCoveringViewVisibility(visible: Bool) {
        if let view = translucentCoveringView where visible == view.hidden {
            // Set the start point of animation to start making the view visible
            if visible == true {
                view.alpha = 0;
                view.hidden = false
            }
            // Do the animation
            UIView.animateWithDuration(0.25,
                animations: {
                    view.alpha = visible ? 1 : 0
                },
                completion: { finished in
                    view.hidden = !visible
                }
            )
        }
    }
    
    @objc private func handleScrollViewDoubleTap(recognizer: UITapGestureRecognizer) {
        guard let scv = scrollView else {
            return
        }
        
        // Do nothing if the tapped point is not in the image view frame
        guard CGRectContainsPoint(imageView.frame, recognizer.locationInView(scv)) else {
            return
        }
        
        let pointInView = recognizer.locationInView(imageView)
        
        // Should zoom in, to the maximum zoom scale regarding the tapped point
        if scv.zoomScale < scv.maximumZoomScale {
            let rectToZoomSize = CGSize(width: scv.bounds.width, height: scv.bounds.height)
            let rectToZoomPoint = CGPoint(
                x: pointInView.x - (rectToZoomSize.width / 2),
                y: pointInView.y - (rectToZoomSize.height / 2))
            
            scv.zoomToRect(CGRect(origin: rectToZoomPoint, size: rectToZoomSize), animated: true)
        }
        // Should zoom out, to minimum zoom scale
        else {
            scv.setZoomScale(scv.minimumZoomScale, animated: true)
        }
    }
    
    private func dateString(forDate date: NSDate?) -> String? {
        guard date != nil else {
            return nil
        }
        
        let timeIntervalSinceNow = date!.timeIntervalSinceNow
        
        // The difference is minutes, less than an hour
        var unit = "minute"
        var difference = ceil(timeIntervalSinceNow / 60) * -1
        
        // The difference is hours, less than a day
        if difference > 60 {
            unit = "hour"
            difference = ceil(difference / 60) * -1
            
            // The difference is more than a day, so show the full date and time
            if difference > 24 {
                let dateFormatter = NSDateFormatter()
                dateFormatter.doesRelativeDateFormatting = true
                dateFormatter.dateStyle = NSDateFormatterStyle.MediumStyle
                dateFormatter.timeStyle = NSDateFormatterStyle.ShortStyle
                
                return dateFormatter.stringFromDate(date!)
            }
        }
        // The difference is more than one, so unit should be plural
        if difference > 1 {
            unit += "s"
        }
        
        return "\(max(Int(difference), 1)) \(unit) ago".uppercaseString
    }
    
}

extension PhotoViewController: UIScrollViewDelegate {
    
    func viewForZoomingInScrollView(scrollView: UIScrollView) -> UIView? {
        return self.imageView
    }
    
    func scrollViewDidZoom(scrollView: UIScrollView) {
        centerScrollViewContent()
    }
    
    func scrollViewWillBeginZooming(scrollView: UIScrollView, withView view: UIView?) {
        setTranslucentCoveringViewVisibility(false)
    }
    
    func scrollViewDidEndZooming(scrollView: UIScrollView, withView view: UIView?, atScale scale: CGFloat) {
        let shoudHide = scrollView.zoomScale > scrollView.minimumZoomScale
        setTranslucentCoveringViewVisibility(!shoudHide)
    }
    
}

extension String {
    func heightWithConstrainedWidth(width: CGFloat, font: UIFont) -> CGFloat {
        let constraintRect = CGSize(width: width, height: CGFloat.max)
        let boundingBox = self.boundingRectWithSize(constraintRect, options: NSStringDrawingOptions.UsesLineFragmentOrigin, attributes: [NSFontAttributeName: font], context: nil)
        
        return boundingBox.height
    }
}