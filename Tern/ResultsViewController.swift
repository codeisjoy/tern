//
//  ResultsViewController.swift
//  Tern
//
//  Created by Emad A on 3/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

class ResultsViewController: UIViewController, UIGestureRecognizerDelegate {
    
    @IBOutlet var collectionView: ThumbnailsCollectionView?
    @IBOutlet var countLabelBar: UILabel?
    
    var search: String?
    var serviceTask: NSURLSessionDataTask?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = search?.uppercaseString ?? "Results"
        
        let backButtonItem = UIBarButtonItem(image: UIImage(named: "BackIcon"), style: .Plain, target: self, action: "popViewController")
        backButtonItem.tintColor = UIColor.blackColor()
        navigationItem.leftBarButtonItem = backButtonItem
        
        navigationController?.interactivePopGestureRecognizer?.delegate = self
        navigationController?.interactivePopGestureRecognizer?.enabled = true
        
        countLabelBar?.text = "Searching ..."
        
        if let tcv = collectionView {
            tcv.delegate = self
            tcv.collectionView.contentInset = UIEdgeInsets(top: 44, left: 0, bottom: 0, right: 0)
            
            tcv.delegate?.thumbnailsCollectionViewWillRefresh(tcv)
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if serviceTask?.state == .Running {
            serviceTask?.cancel()
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowPhotoDetails" {
            if let destination = segue.destinationViewController as? PhotoViewController {
                destination.photo = collectionView?.selectedPhoto
            }
        }
    }
    
    @objc private func popViewController() {
        navigationController?.popViewControllerAnimated(true)
    }
}

extension ResultsViewController: ThumbnailsCollectionViewDelegate {
    
    func thumbnailsCollectionViewWillRefresh(collectionView: ThumbnailsCollectionView) {
        guard let text = search else {
            return
        }
        
        collectionView.beginRefreshing()
        
        if serviceTask?.state == .Running {
            serviceTask?.cancel()
        }
        serviceTask = Service.defaultService().askForPhotos(text: text, page: 1, perpage: 500, completion: { [unowned self] photos, error in
            collectionView.completeRefreshing(photos)
            dispatch_async(dispatch_get_main_queue(), { [unowned self] in
                if error != nil {
                    self.countLabelBar?.text = "Can't show results :("
                }
                else {
                    self.countLabelBar?.text = "\(Int(photos!.count)) photos"
                }
            })
        })
        serviceTask?.resume()
    }
    
    func thumbnailsCollectionView(collectionView: ThumbnailsCollectionView, didSelectPhoto photo: Photo) {
        performSegueWithIdentifier("ShowPhotoDetails", sender:self)
    }
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if var frame = countLabelBar?.frame {
            let offset = (scrollView.contentOffset.y + frame.height) * -1
            frame.origin.y = min(offset, 0)
            countLabelBar?.frame = frame
        }
    }
    
}
