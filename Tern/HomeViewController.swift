//
//  HomeViewController.swift
//  Tern
//
//  Created by Emad A on 31/10/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    @IBOutlet var searchBar: UISearchBar?
    @IBOutlet var collectionView: ThumbnailsCollectionView?
    
    private lazy var dimBackgroundView: UIVisualEffectView = {
        let effect = UIBlurEffect(style: .Dark)
        let effectView = UIVisualEffectView(effect: effect)
        effectView.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        
        let button = UIButton(type: .Custom)
        button.addTarget(self, action: "dimBackgroundDidTouchUpInside", forControlEvents: .TouchUpInside)
        button.autoresizingMask = [.FlexibleWidth, .FlexibleHeight]
        button.frame = effectView.bounds
        
        effectView.contentView.addSubview(button)
        
        return effectView
        }()
    
    private var serviceTask: NSURLSessionDataTask?
    
    // MARK: - Overriden Methods
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Show the logo in title bar
        let logoImage = UIImage(named: "LogoImage")
        self.navigationItem.titleView = UIImageView(image: logoImage)
        
        // Set the search bar text colour globaly, as there is no access to set that just for the instance
        UITextField.appearanceWhenContainedInInstancesOfClasses([UISearchBar.classForCoder()]).textColor = UIColor.whiteColor()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tcv = collectionView {
            tcv.delegate = self
            tcv.delegate?.thumbnailsCollectionViewWillRefresh(tcv)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        searchBar?.text = nil
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (serviceTask?.state == .Running) {
            serviceTask?.cancel()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowPhotoDetails" {
            if let destination = segue.destinationViewController as? PhotoViewController {
                destination.photo = collectionView?.selectedPhoto
            }
        }
        else if segue.identifier == "ShowResults" {
            if let destination = segue.destinationViewController as? ResultsViewController {
                destination.search = searchBar?.text
            }
        }
    }
    
    // MARK: - Private Methods
    
    func dimBackgroundDidTouchUpInside() {
        searchBar?.resignFirstResponder()
    }
    
    func dimsBackground(dims: Bool, forSearchBar searchBar: UISearchBar) {
        let curtain = dimBackgroundView
        // Prepare the curtain view to be shown, in case it is not
        if dims {
            curtain.frame = CGRect(
                origin: CGPoint(x: CGRectGetMinX(searchBar.frame), y: CGRectGetMaxY(searchBar.frame)),
                size: CGSize(width: searchBar.bounds.width, height: view.bounds.height - CGRectGetMaxY(searchBar.frame)))
            if curtain.superview == nil {
                view.addSubview(curtain)
            }
        }
        else {
            curtain.removeFromSuperview()
        }
    }
    
}

// MARK: - UISerachBarDelegate Extension

extension HomeViewController: UISearchBarDelegate {
    
    func searchBarTextDidBeginEditing(searchBar: UISearchBar) {
        dimsBackground(true, forSearchBar: searchBar)
    }
    
    func searchBarTextDidEndEditing(searchBar: UISearchBar) {
        dimsBackground(false, forSearchBar: searchBar)
    }
    
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        performSegueWithIdentifier("ShowResults", sender: self)
    }
    
}

// MARK: - ThumbnailsCollectionViewDelegate Extension

extension HomeViewController: ThumbnailsCollectionViewDelegate {
    
    func thumbnailsCollectionViewWillRefresh(collectionView: ThumbnailsCollectionView) {
        collectionView.beginRefreshing()
        
        if serviceTask?.state == .Running {
            serviceTask?.cancel()
        }
        serviceTask = Service.defaultService().askForLatestPublicPhotos(page: 1, perpage: 20) { photos, error in
            collectionView.completeRefreshing(photos)
        }
        serviceTask?.resume()
    }
    
    func thumbnailsCollectionView(collectionView: ThumbnailsCollectionView, didSelectPhoto photo: Photo) {
        performSegueWithIdentifier("ShowPhotoDetails", sender:self)
    }
    
}