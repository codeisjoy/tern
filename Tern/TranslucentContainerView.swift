//
//  TranslucentContainerView.swift
//  Tern
//
//  Created by Emad A on 3/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

class TranslucentContainerView: UIView {
    
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let view = super.hitTest(point, withEvent: event)
        return view == self ? nil : view
    }
    
}
