//
//  Photo.swift
//  Tern
//
//  Created by Emad A on 1/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import Foundation

struct Photo {
    
    private(set) var uid: String
    private(set) var title: String
    private(set) var owner: Person
    
    private(set) var largeViewURL: NSURL
    private(set) var thumbnailURL: NSURL
    
    private(set) var dateUploaded: NSDate?
    
    // MARK: - Public Methods
    
    init?(photo: [String: AnyObject]) {
        // Small photos in terms of dimention will be rejected.
        guard let zURLString = photo["url_z"] as? String else {
            return nil
        }
        
        uid = photo["id"] as! String
        
        if let title = photo["title"] as? String where title.characters.count > 0{
            self.title = title
        }
        else {
            self.title = "Untitled"
        }
        
        owner = Person(
            nsid: photo["owner"] as! String,
            realName: photo["ownername"] as? String ?? "--")
        
        // In the worst case scenario large view photo is the thumbnail
        var largeViewURLString = zURLString
        // The order is important, to get the largest avaialble photo
        for url: String in ["url_k", "url_h", "url_b", "url_c"] {
            if let urlString = photo[url] as? String {
                largeViewURLString = urlString
                break
            }
        }
        
        thumbnailURL = NSURL(string: zURLString)!
        largeViewURL = NSURL(string: largeViewURLString)!
        
        if let string = photo["dateupload"] as? String, timeInterval = NSTimeInterval(string) {
            dateUploaded = NSDate(timeIntervalSince1970: timeInterval)
        }
    }
    
}