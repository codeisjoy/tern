//
//  Person.swift
//  Tern
//
//  Created by Emad A on 1/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import Foundation

struct Person {
    
    private(set) var nsid: String
    private(set) var realName: String
    
    init(nsid: String, realName: String) {
        self.nsid = nsid
        self.realName = realName
    }
    
}