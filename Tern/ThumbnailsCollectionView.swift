//
//  ThumbnailsCollectionView.swift
//  Tern
//
//  Created by Emad A on 1/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

protocol ThumbnailsCollectionViewDelegate: UIScrollViewDelegate {
    func thumbnailsCollectionViewWillRefresh(collectionView: ThumbnailsCollectionView)
    func thumbnailsCollectionView(collectionView: ThumbnailsCollectionView, didSelectPhoto photo: Photo)
}

class ThumbnailsCollectionView: UIView {
    
    // MARK: - Private Constants
    
    private let columnsCount: CGFloat = 3
    private let cellReuseId = "ThumbnailCell"
    
    // MARK: Public Variables
    
    var delegate: ThumbnailsCollectionViewDelegate?
    var selectedPhoto: Photo? {
        get {
            if let indexPath = self.collectionView.indexPathsForSelectedItems()?.first {
                return self.data?[indexPath.row]
            }
            return nil
        }
    }
    
    // MARK: Private Variables
    
    private(set) var collectionView: UICollectionView
    
    private var collectionViewLayout: UICollectionViewFlowLayout? {
        get {
            return collectionView.collectionViewLayout as? UICollectionViewFlowLayout
        }
    }
    
    private var refreshControl: UIRefreshControl = UIRefreshControl()
    
    typealias LayoutFactors = (size: CGSize, gap: CGFloat)
    private var layoutFactors: LayoutFactors = (size: CGSizeZero, gap: 0)
    
    private var data: [Photo]?
    
    // MARK: - Overriden Methods
    
    required init?(coder aDecoder: NSCoder) {
        // Initialise the collection view
        
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: layout)
        
        super.init(coder: aDecoder)
        
        backgroundColor = UIColor.clearColor()
        
        // Customise collection view
        
        collectionView.addSubview(refreshControl)
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.alwaysBounceVertical = true
        collectionView.backgroundColor = UIColor.whiteColor()
        collectionView.registerClass(ThumbnailsCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: cellReuseId)
        
        addSubview(collectionView)
        
        // Set refresh control handler
        
        refreshControl.addTarget(self, action: "delegateWillRefresh", forControlEvents: .ValueChanged)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Refresh the collection view layout item spacing
        var gap: CGFloat = 3 // minimum gap between items
        let dim: CGFloat = floor((bounds.width - gap * (columnsCount - 1)) / columnsCount)
        // Recalculate the gap regarding items dimension
        gap = (bounds.width - dim * columnsCount) / (columnsCount - 1)
        
        collectionViewLayout?.minimumLineSpacing = gap
        collectionViewLayout?.minimumInteritemSpacing = gap
        collectionViewLayout?.itemSize = CGSize(width: dim, height: dim)
        
        // Set the collection view frame
        
        collectionView.frame = bounds
    }
    
    // MARK: - Public Methods
    
    func beginRefreshing() {
        refreshControl.beginRefreshing()
    }
    
    func completeRefreshing(data: [Photo]?) {
        self.data = data
        dispatch_async(dispatch_get_main_queue()) { [unowned self] in
            self.refreshControl.endRefreshing()
            self.collectionView.reloadSections(NSIndexSet(index: 0))
        }
    }
    
    // MARK: - Private Methods
    
    private func generateRandomColor() -> UIColor {
        let hue : CGFloat = CGFloat(arc4random() % 256) / 256
        let saturation : CGFloat = CGFloat(arc4random() % 128) / 256
        let brightness : CGFloat = CGFloat(arc4random() % 128) / 256 + 0.5
        
        return UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: 0.1)
    }
    
    @objc private func delegateWillRefresh() {
        delegate?.thumbnailsCollectionViewWillRefresh(self)
    }
    
}

extension ThumbnailsCollectionView: UICollectionViewDataSource {
    
    func collectionView(
        collectionView: UICollectionView,
        numberOfItemsInSection section: Int) -> Int
    {
        return data?.count ?? 0
    }
    
    func collectionView(
        collectionView: UICollectionView,
        cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellReuseId, forIndexPath: indexPath) as! ThumbnailsCollectionViewCell
        cell.backgroundColor = generateRandomColor()
        if let photo = data?[indexPath.item] {
            cell.showPhotoThumbnail(photo)
        }
        
        return cell
    }
    
}

extension ThumbnailsCollectionView: UICollectionViewDelegate {
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        if let photo = data?[indexPath.item] {
            delegate?.thumbnailsCollectionView(self, didSelectPhoto: photo)
        }
    }
    
}

extension ThumbnailsCollectionView: UIScrollViewDelegate {
    
    func scrollViewDidScroll(scrollView: UIScrollView) {
        delegate?.scrollViewDidScroll?(scrollView)
    }
    
}