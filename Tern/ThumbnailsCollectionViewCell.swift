//
//  ThumbnailsCollectionViewCell.swift
//  Tern
//
//  Created by Emad A on 2/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import UIKit

class ThumbnailsCollectionViewCell: UICollectionViewCell {
    
    // MARK: - Public Properties
    
    var image: UIImage? {
        didSet{
            dispatch_async(dispatch_get_main_queue()) { [unowned self] in
                self.imageView.image = self.image
                if self.imageView.alpha == 0 {
                    UIView.animateWithDuration(0.25, animations: { () -> Void in
                        self.imageView.alpha = 1
                    })
                }
            }
        }
    }
    
    // MARK: - Private Properties
    
    private let imageView = UIImageView()
    private var imageDownloadTask: NSURLSessionDownloadTask?
    
    // MARK: Overriden Methods
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        // Initialise the image view
        imageView.contentMode = UIViewContentMode.ScaleAspectFill
        imageView.clipsToBounds = true
        contentView.addSubview(imageView)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        // Set the image view frame
        imageView.frame = contentView.bounds
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        if let task = imageDownloadTask where task.state == .Running {
            imageDownloadTask?.cancel()
        }
    }
    
    // MARK: Public Methods
    
    func showPhotoThumbnail(photo: Photo) {
        self.image = nil
        
        // Find the cached file path for given photo
        let cachDirectoryPath = NSSearchPathForDirectoriesInDomains(.CachesDirectory, .UserDomainMask, true).last
        let cachedFilePath = NSURL(string: cachDirectoryPath!)!.URLByAppendingPathComponent(photo.uid).absoluteString
        
        // If given photo has a cached thumbnail then set use the cashed file
        if NSFileManager.defaultManager().fileExistsAtPath(cachedFilePath) {
            self.imageView.alpha = 1
            self.image = UIImage(contentsOfFile: cachedFilePath)
            return
        }
        
        let config = NSURLSessionConfiguration.defaultSessionConfiguration()
        config.requestCachePolicy = NSURLRequestCachePolicy.ReturnCacheDataElseLoad
        
        let session = NSURLSession(configuration: config)
        
        // There is no cashed file for this photo ...
        imageDownloadTask = session.downloadTaskWithURL(photo.thumbnailURL, completionHandler: { [unowned self] tmpURL, response, error in
            guard error == nil else {
                return
            }
            
            // All good, so cache the downloaded image data and then show the image
            if let imageURL = tmpURL, imageData = NSData(contentsOfURL: imageURL) {
                imageData.writeToFile(cachedFilePath, atomically: true)
                self.imageView.alpha = 0
                self.image = UIImage(data: imageData)
            }
            // There is something wrong on showing the thumbnail
            else {
                
            }
        })
        imageDownloadTask?.resume()
    }
    
}
