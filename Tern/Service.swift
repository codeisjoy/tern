//
//  Service.swift
//  Tern
//
//  Created by Emad A on 1/11/2015.
//  Copyright © 2015 Code is joy!. All rights reserved.
//

import Foundation

class Service {
    
    private static let staticInstance = Service()
    
    private let baseURL = "https://api.flickr.com/services/rest"
    private let APIKey = "eebad9c2a645a07e5b3d4190de209483"
    
    private let session = NSURLSession.sharedSession()
    
    private var defaultURLComponents: NSURLComponents? {
        let components = NSURLComponents(string: baseURL)!
        components.queryItems = [
            NSURLQueryItem(name: "api_key", value: APIKey),
            NSURLQueryItem(name: "format", value: "json"),
            NSURLQueryItem(name: "nojsoncallback", value: "1"),
            NSURLQueryItem(name: "extras", value: "date_upload,owner_name,url_k,url_h,url_b,url_c,url_z")
        ]
        
        return components
    }
    
    // MARK: - Public Methods
    
    class func defaultService() -> Service {
        return staticInstance
    }
    
    func askForLatestPublicPhotos(page page: UInt = 1, perpage: UInt = 100, completion: ([Photo]?, NSError?) -> ()) -> NSURLSessionDataTask {
        let defaultComponents = defaultURLComponents
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "method", value: "flickr.photos.getRecent"))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "per_page", value: "\(perpage)"))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "page", value: "\(page)"))
        
        guard let taskURL = defaultComponents?.URL else {
            fatalError("Request for Latest Public Photos has no valid URL.")
        }
        
        return dataTask(withURL: taskURL, completion: completion)
    }
    
    func askForPhotos(text text: String, page: UInt = 1, perpage: UInt = 100, completion: ([Photo]?, NSError?) -> ()) -> NSURLSessionDataTask {
        let defaultComponents = defaultURLComponents
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "method", value: "flickr.photos.search"))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "per_page", value: "\(perpage)"))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "page", value: "\(page)"))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "text", value: text))
        defaultComponents?.queryItems?.append(NSURLQueryItem(name: "content_type", value: "\(1)"))
        
        guard let taskURL = defaultComponents?.URL else {
            fatalError("Request for searcing \"\(text)\" has no valid URL.")
        }
        
        return dataTask(withURL: taskURL, completion: completion)
    }
    
    // MARK: - Private Methods
    
    func dataTask(withURL url: NSURL, completion: ([Photo]?, NSError?) -> ()) -> NSURLSessionDataTask {
        return session.dataTaskWithURL(url) { data, response, error in
            guard error == nil else {
                completion(nil, error)
                return
            }
            
            do {
                let json = try NSJSONSerialization.JSONObjectWithData(data!, options: .AllowFragments) as! [String: AnyObject]
                if let photos = json["photos"], photo = photos["photo"] as? [[String: AnyObject]] {
                    var result: [Photo] = []
                    for item in photo {
                        if let model = Photo(photo: item) {
                            result.append(model)
                        }
                    }
                    completion(result, nil)
                }
                else {
                    let e = NSError(domain: "AppDomain", code: -1, userInfo: nil)
                    completion(nil, e)
                }
            }
            catch _ {
                let e = NSError(domain: "AppDomain", code: -1, userInfo: nil)
                completion(nil, e)
            }
        }
    }
    
}
